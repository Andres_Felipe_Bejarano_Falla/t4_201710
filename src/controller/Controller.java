package controller;

import model.data_structures.ListaDobleEncadenada;
import model.logic.GeneradorDeMuestrasAleatoriasArray;
import model.logic.GeneradorDeMuestrasAleatoriasList;
import model.logic.QuickSortArray;
import model.logic.QuickSortList;
import model.vo.VOPelicula;

public class Controller {

	private static GeneradorDeMuestrasAleatoriasList generadorList = new GeneradorDeMuestrasAleatoriasList();
	private static GeneradorDeMuestrasAleatoriasArray generadorArray = new GeneradorDeMuestrasAleatoriasArray();
	private static QuickSortList<VOPelicula> quickList = new QuickSortList<>();
	private static QuickSortArray<VOPelicula> quickArray = new QuickSortArray<>();
	
	public static int generaryOrdenarList(int n){
		ListaDobleEncadenada<VOPelicula> v = (ListaDobleEncadenada<VOPelicula>) generadorList.generarLista(n);
		long i = System.currentTimeMillis();
		quickList.sort(v);
		int time = (int) (System.currentTimeMillis()-i);
		return time;	
	}
	
	public static int generaryOrdenarArray(int n){
		VOPelicula[] v = generadorArray.generarLista(n);
		long i = System.currentTimeMillis();
		quickArray.sort(v);
		int time = (int) (System.currentTimeMillis()-i);
		return time;	
	}
}
