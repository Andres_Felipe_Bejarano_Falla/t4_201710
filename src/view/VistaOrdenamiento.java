package view;

import java.util.Scanner;

import controller.Controller;

public class VistaOrdenamiento {
	
	public static void main(String[] args) {
	
		
		Scanner scan = new Scanner(System.in);
		boolean f = false;
		while(!f){
			printMenu();
			
			int option = scan.nextInt();
			
			switch(option){
			
			case 1:
				System.out.println("Ingrese el n�mero de elementos de la lista deseada:");
				int n1 = scan.nextInt();
				System.out.println("El tiempo que tarda en ordenar es:");
				System.out.println(Controller.generaryOrdenarArray(n1));
				System.out.println("------------------------------------------------------------");
				System.out.println("");
				break;
			case 2:
				System.out.println("Ingrese el n�mero de elementos de la lista deseada:");
				int n2 = scan.nextInt();
				System.out.println("El tiempo que tarda en ordenar es:");
				System.out.println(Controller.generaryOrdenarList(n2));
				System.out.println("------------------------------------------------------------");
				System.out.println("");
				break;
			case 3:
				f = true;
				break;
			
			}
			
		}
	}
	
	public static void printMenu(){
		System.out.println("------------------------------------------------------------");
		System.out.println("--------------ISIS 1206 - Estructuras de Datos--------------");
		System.out.println("----------------------------Taller 4------------------------");
		System.out.println("1. Generar y Ordenar un ARRAY con QuickSort");
		System.out.println("2. Generar y Ordenar un LIST con QuickSort");
		System.out.println("3. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
	}
}
