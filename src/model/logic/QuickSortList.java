package model.logic;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.vo.VOPelicula;

public class QuickSortList<T extends Comparable<T>> {

	public void sort(ILista<T> a ){
		T[] v = (T[]) new VOPelicula[a.darNumeroElementos()];
		for(int i = 0; i< a.darNumeroElementos(); i++)	v[i]= a.darElemento(i);	
		List<T> lista = Arrays.asList(v);
		Collections.shuffle(lista);
		v = lista.toArray(v);
		a = new ListaDobleEncadenada<T>(v);
		sort(a, 0, a.darNumeroElementos()-1);
	}

	public void sort(ILista<T> a ,int lo, int hi){
		if(hi <= lo) return;
		int j = partition(a, lo, hi);
		sort(a, lo, j-1);
		sort(a, j+1, hi);
	}
	
	public int partition(ILista<T> lista, int lo, int hi) {
		int i = lo, j = hi + 1;
		T p = lista.darElemento(lo);
		while (true) {
			while (less(lista.darElemento(++i), p))if (i == hi)break;
			while (less(p, lista.darElemento(--j)))if (j == lo)break;
			if (i >= j)break;
			exch(lista, i, j);
		}
		exch(lista, lo, j);
		return j;
	}

	public void exch(ILista<T> lista, int a, int b) {
		T t = lista.darElemento(a);
		lista.setItem(lista.darElemento(b),a);
		lista.setItem(t,b);
	}

	public boolean less(T a, T b) {
		return a.compareTo(b) < 0;
	}

	public void show(ILista<T> a) {
		for (T t : a)
			System.out.print(t + "  ");
		System.out.println();
	}

	public boolean isSorted(ILista<T> a) {
		for (int i = 0; i < a.darNumeroElementos(); i++)
			if (less(a.darElemento(i),a.darElemento(i-1)))
				return false;
		return true;
	}

}
