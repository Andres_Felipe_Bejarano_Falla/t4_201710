package model.logic;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class QuickSortArray<T extends Comparable<T>> {

	public void sort(T[] a ){
		List<T> lista = Arrays.asList(a);
		Collections.shuffle(lista);
		a = lista.toArray(a);
		sort(a, 0, a.length-1);
	}

	public void sort(T[] a ,int lo, int hi){
		if(hi <= lo) return;
		int j = partition(a, lo, hi);
		sort(a, lo, j-1);
		sort(a, j+1, hi);
	}
	
	public int partition(T[] lista, int lo, int hi) {
		int i = lo, j = hi + 1;
		T p = lista[lo];
		while (true) {
			while (less(lista[++i], p))if (i == hi)break;
			while (less(p, lista[--j]))if (j == lo)break;
			if (i >= j)break;
			exch(lista, i, j);
		}
		exch(lista, lo, j);
		return j;
	}

	public void exch(T[] lista, int a, int b) {
		T t = lista[a];
		lista[a] = lista[b];
		lista[b] = t;
	}

	public boolean less(T a, T b) {
		return a.compareTo(b) < 0;
	}

	public void show(T[] a) {
		for (T t : a)
			System.out.print(t + "  ");
		System.out.println();
	}

	public boolean isSorted(T[] a) {
		for (int i = 0; i < a.length; i++)
			if (less(a[i], a[i - 1]))
				return false;
		return true;
	}

}
