package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Random;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.vo.VOPelicula;

public class GeneradorDeMuestrasAleatoriasList{

	
	private static final String RUTA_GENERADOR = "./data/movies.csv";
	
	private ListaDobleEncadenada<VOPelicula> origin;
	
	private ListaDobleEncadenada<VOPelicula> listaN;
	
	public GeneradorDeMuestrasAleatoriasList() {
		// TODO Auto-generated constructor stub
		origin = null;
		listaN = null;
	}
	
	public ILista<VOPelicula> generarLista(int n){
		origin = new ListaDobleEncadenada<>();
		listaN = new ListaDobleEncadenada<>();
		cargarPeliculasBase();
		int t = origin.darNumeroElementos();
		int i =0;
		Random r = new Random();
		while(i<n)	{
			double m = r.nextDouble();
			listaN.agregarElementoFinal(origin.darElemento((int)m*t));
			i++;
		}
		return listaN;
	}
	
	public void cargarPeliculasBase(){
			BufferedReader buff = null;
			String sepCvs = ",";
			String line = "";
			try {
				buff = new BufferedReader(new FileReader(RUTA_GENERADOR));
				line = buff.readLine();
				while ((line = buff.readLine()) != null) {
					String[] datos = line.split(sepCvs);
					String movieId = datos[0];
					String generos = datos[datos.length - 1];
					String nombre = "";
					for (int i = 1; i < datos.length - 1; i++) {
						nombre = nombre + datos[i];
					}

					if (nombre.startsWith("\"") && nombre.endsWith("\"")) {
						nombre = nombre.substring(1, nombre.length() - 1);
					}

					int a�o = -1;

					if (nombre.endsWith(")")) {
						Character g = nombre.charAt(nombre.length() - 2);
						if (g.isDigit(g)) {
							a�o = Integer.parseInt(nombre.substring(nombre.length() - 5, nombre.length() - 1));
						} else {
							a�o = Integer.parseInt(nombre.substring(nombre.length() - 6, nombre.length() - 2));
						}
					}
					nombre = nombre.substring(0, nombre.length() - 6);
					VOPelicula p = new VOPelicula();
					p.setAgnoPublicacion(a�o);
					p.setGenerosAsociados(new ListaDobleEncadenada<>(generos.split("|")));
					p.setTitulo(nombre);
					origin.agregarElementoFinal(p);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
	}
	
	
}
