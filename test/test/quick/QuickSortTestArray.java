package test.quick;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

import junit.framework.TestCase;
import model.data_structures.Queue;
import model.logic.GeneradorDeMuestrasAleatoriasArray;
import model.logic.QuickSortArray;
import model.vo.VOPelicula;

public class QuickSortTestArray extends TestCase{
	
	private VOPelicula[] prueba;
	private VOPelicula[] ordenado;
	private static GeneradorDeMuestrasAleatoriasArray g = new GeneradorDeMuestrasAleatoriasArray();
	private static final int TAMA�O = 50000;
	
	public void setupEscenario1(){
		prueba= g.generarLista(TAMA�O);
		List<VOPelicula> lista = Arrays.asList(prueba);
		Collections.sort(lista);
		prueba = lista.toArray(prueba);
		ordenado = prueba;
	}
	
	public void setupEscenario2(){
		prueba= g.generarLista(TAMA�O);
		List<VOPelicula> lista = Arrays.asList(prueba);
		Collections.reverse(lista);
		prueba = lista.toArray(prueba);
		ordenado = prueba;
	}
	
	public void setupEscenario3(){
		prueba= g.generarLista(TAMA�O);
		List<VOPelicula> lista = Arrays.asList(prueba);
		Collections.shuffle(lista);
		prueba = lista.toArray(prueba);
		ordenado = prueba;
	}
	
	public void testSort1(){
		setupEscenario1();
		QuickSortArray<VOPelicula> quickSort=new QuickSortArray<>();
		quickSort.sort(prueba);
		Arrays.sort(ordenado);
		for (int i = 0; i < ordenado.length; i++) {
			assertEquals(prueba[i],ordenado[i] );
		}
	}
	
	public void testSort2(){
		setupEscenario2();
		QuickSortArray<VOPelicula> quickSort=new QuickSortArray<>();
		quickSort.sort(prueba);
		Arrays.sort(ordenado);
		for (int i = 0; i < ordenado.length; i++) {
			assertEquals(prueba[i],ordenado[i] );
		}
	}
	
	public void testSort3(){
		setupEscenario3();
		QuickSortArray<VOPelicula> quickSort=new QuickSortArray<>();
		quickSort.sort(prueba);
		Arrays.sort(ordenado);
		for (int i = 0; i < ordenado.length; i++) {
			assertEquals(prueba[i],ordenado[i] );
		}
	}
	
	
}
