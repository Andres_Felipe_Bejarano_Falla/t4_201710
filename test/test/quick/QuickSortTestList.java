package test.quick;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

import junit.framework.TestCase;
import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.Queue;
import model.logic.GeneradorDeMuestrasAleatoriasList;
import model.logic.QuickSortList;
import model.vo.VOPelicula;

public class QuickSortTestList extends TestCase{
	
	private ListaDobleEncadenada<VOPelicula> prueba;
	private VOPelicula[] ordenado;
	private static GeneradorDeMuestrasAleatoriasList g = new GeneradorDeMuestrasAleatoriasList();
	private static final int TAMA�O = 1000;
	
	public void setupEscenario1(){
		prueba= (ListaDobleEncadenada<VOPelicula>) g.generarLista(TAMA�O);
		VOPelicula[] v = new VOPelicula[prueba.darNumeroElementos()];
		for(int i = 0; i< prueba.darNumeroElementos(); i++)	v[i]=prueba.darElemento(i);	
		List<VOPelicula> lista = Arrays.asList(v);
		Collections.sort(lista);
		v = lista.toArray(v);
		prueba = new ListaDobleEncadenada<VOPelicula>(v);
		ordenado = v;
	}
	
	public void setupEscenario2(){
		prueba= (ListaDobleEncadenada<VOPelicula>) g.generarLista(TAMA�O);
		VOPelicula[] v = new VOPelicula[prueba.darNumeroElementos()];
		for(int i = 0; i< prueba.darNumeroElementos(); i++)	v[i]=prueba.darElemento(i);	
		List<VOPelicula> lista = Arrays.asList(v);
		Collections.reverse(lista);
		v = lista.toArray(v);
		prueba = new ListaDobleEncadenada<VOPelicula>(v);
		ordenado = v;
	}
	
	public void setupEscenario3(){
		prueba= (ListaDobleEncadenada<VOPelicula>) g.generarLista(TAMA�O);
		VOPelicula[] v = new VOPelicula[prueba.darNumeroElementos()];
		for(int i = 0; i< prueba.darNumeroElementos(); i++)	v[i]=prueba.darElemento(i);	
		List<VOPelicula> lista = Arrays.asList(v);
		Collections.shuffle(lista);
		v = lista.toArray(v);
		prueba = new ListaDobleEncadenada<VOPelicula>(v);
		ordenado = v;
	}
	
	public void testSort1(){
		setupEscenario1();
		QuickSortList<VOPelicula> quickSort=new QuickSortList<>();
		quickSort.sort(prueba);
		Arrays.sort(ordenado);
		for (int i = 0; i < ordenado.length; i++) {
			assertEquals(prueba.darElemento(i).getTitulo(),ordenado[i].getTitulo() );
		}
	}

	public void testSort2(){
		setupEscenario2();
		QuickSortList<VOPelicula> quickSort=new QuickSortList<>();
		quickSort.sort(prueba);
		Arrays.sort(ordenado);

		for (int i = 0; i < ordenado.length; i++) {
			assertEquals(prueba.darElemento(i).getTitulo(),ordenado[i].getTitulo() );
		}
	}
	
	public void testSort3(){
		setupEscenario3();
		QuickSortList<VOPelicula> quickSort=new QuickSortList<>();
		quickSort.sort(prueba);
		Arrays.sort(ordenado);

		for (int i = 0; i < ordenado.length; i++) {
			assertEquals(prueba.darElemento(i).getTitulo(),ordenado[i].getTitulo() );
		}
	}
	
	
}
